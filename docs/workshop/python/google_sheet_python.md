# Google Sheet with Python

This is workshop is based on:  
- [gspread](https://docs.gspread.org/en/latest/index.html#)  
- [medium](https://lcalcagni.medium.com/how-to-manipulate-google-spreadsheets-using-python-b15657e6ed0d)  
- [youtube](https://www.youtube.com/watch?v=sVURhxyc6jE)  

In this workshop, I will explain how can we manipulate google sheets using Python.  

- Step 1: Sign up for a google account 
- Step 2: Create a Google Service Account
     - 2.1 Create a new project on Google Cloud Platform
     - 2.2 Enable the required APIs
     - 2.3 Manage Google Sheets API credentials
- Step 3: Share your Google Sheet with your Service Account
- Step 4: Manipulate your Google Sheet with Python

## Step 1: Create a Google Service Account
First, we will have to create a Service Account. This account will be used to make authorized API calls to Google Cloud Services. It is important to mention that to create a Service Account it is required to have a Google account first.

### 1.1 Create a new project on Google Cloud Platform  
1. Go to the developer’s console and click on “Select a project."
2. Click on “New project” to create a new project.
3. Fill in the required fields and then click on “To Create”.

### 1.2 Enable the required APIs
Now that the new project is created, we need to enable two APIs:  
- Google Drive API  
- Google Sheets API  
To do this, on the top bar of the console, where “Find Products and Resources” (or “Search products and resources”) is displayed, type “Google Drive API” and select the first option.

### 1.3 Manage Google Sheets API credentials
Now that both APIs are enabled, we will add credentials for the Google Sheets API:  
1. Search again “Google Sheets API” on the top bar and click on “Manage”.  
2. Go to “Credentials” on the left menu and then click on “Manage service accounts”.  

